import {createClient, groq} from 'next-sanity';
import createImageBuilder from "@sanity/image-url";

const projectId = process.env.NEXT_PUBLIC_SANITY_PROJECT_ID // "pv8y60vp"
const dataset = process.env.NEXT_PUBLIC_SANITY_DATASET // "production"

export const config = {
    projectId: projectId,
    dataset: dataset || "production",
    apiVersion: "2021-10-21", // https://www.sanity.io/docs/api-versioning
};

//Set up the client for fetching data in the getProps page functions
export const sanityClient = createClient(config);

export const urlFor = (source: any) =>
    createImageBuilder(config).image(source);
