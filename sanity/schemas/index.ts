import post from './pageInfo';
import experience from './experience';
import skill from './skill';
import socials from './socials';
import projects from './projects';

export const schemaTypes = [post, experience, skill, socials, projects];
