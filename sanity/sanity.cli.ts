import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'l54cruul',
    dataset: 'production'
  }
})
